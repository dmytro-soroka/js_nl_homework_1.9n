/* 
    TASK 1

    На странице html у нас имеется 5 <div class="div-item"></div>

    Необходимо с помощью Js добавить во внутрь каждого фразу : "Hello world"

    Исходный div изменяться никак не должен.

*/

const divs = document.querySelectorAll('.div-item');

for(let div of divs) {
    div.innerHTML = 'Hello world';
}

/* 
    TASK 2

    У нас есть три элемента:

    <input type="button"/>

    <input type="checkbox"/>

    <input type="file"/>

    Необходимо в файле Js создать пустой массив, и добавить с помощью метода push()

    во внутрь массива все значения атрибута type всех трех кнопок.

    outcome of task :

    [button, checkbox, file];

    */

    const inputs = document.querySelectorAll('input');

    let typeArray = [];

    for (let input of inputs) {
        typeArray.push(input.type)
    }


/* 
    TASK 3

    У нас есть:

    <p>Я скоро буду разработчиком</p>

    Необходимо с помощью Js сделать так, чтобы размер шрифта у текста внутри параграфа

    был 40px, и цвет зеленый.

    Необходимо использовать те знания которые получили на уроке 
    
*/

const p = document.querySelector('p');

p.setAttribute('class', 'text');

/* 
    TASK 4

    На странице html есть кнопка <button>Click me</button>

    И также под кнопкой есть <div>Ты нажал на кнопку</div> - который изначально скрыт от пользователя при помощи css класса

    Необходимо отследить, когда пользователь нажмет на кнопку и показать ему скрытый div.

    Необходимо использовать те знания которые получили на уроке 
    
*/

const btn = document.querySelector('button');
const div = document.getElementById('div');

btn.onclick = function() {
    div.setAttribute('class', 'show');
}

/* 
    TASK 5

    У нас есть пять <a>Ссылка</a> на html странице

    Необходимо добавить к каждой ссылке data attribute в значение которого передать название того сайта куда ведет эта ссылка

    При этом добавление аттрибута href является опциональным

    Далее вам необходимо определить по какой именно ссыkке кликнул пользователь, и вывести значение data атрибута именно этой ссылки в консоль

*/

const links = document.querySelectorAll('a');

for(let link of links) {
    link.onclick = function() {
        console.log(this.dataset.link)
    }
}